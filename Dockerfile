FROM ruby:2.5.7


RUN apt-get update && apt-get upgrade -y
RUN apt-get install apt-transport-https  nano
# Editor needed for edit rails credentials
ENV EDITOR=nano


WORKDIR /usr/src/app

COPY Gemfile* ./
RUN bundle install
#RUN bundle install --deployment --jobs 4  --path /usr/local/bundle --binstubs /usr/local/bundle/bin --without development test
# For update gems run:  bundle install --no-deployment

# Add App-User
#RUN useradd --home-dir $PWD --no-create-home --user-group app && chown -R app:app /usr/src/app
#USER app

COPY . /usr/src/app

ENV RAILS_LOG_TO_STDOUT true
EXPOSE 3000
CMD rm -f tmp/pids/server.pid && rails server -b 0.0.0.0
